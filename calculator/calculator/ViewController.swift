//
//  ViewController.swift
//  calculator
//
//  Created by user on 01.10.16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var displayResultLabel: UILabel!
    
    var stillTyping = false                                             //проверка на первую цифру 0
    var dotIsPlaced = false                                             //переменная проверяет ставили мы точку или нет
    var firstOperand: Double = 0                                        //оператор для присвоения первого числа
    var secondOperand: Double = 0                                       //второй оператор
    var operationSign: String = ""                                      //результат
    
    var currentInput: Double {                                          //переменная, которая переводит из текста лейбла в дабл
        get {
            return Double (displayResultLabel.text!)!
        }
        set {
            let value = "\(newValue)"
            let valueArray = value.components(separatedBy: ".")         // разделяем число до точки и после
            if valueArray[1] == "0" {                                   // проверяем на ноль часть после точки
                displayResultLabel.text = "\(valueArray[0])"
            } else {
            displayResultLabel.text = "\(newValue)"                     // если после точки не ноль - записываем все число
            }
            stillTyping = false
        }
    }
    
    @IBAction func numberPressed(_ sender: UIButton) {
        
        let number = sender.currentTitle!                                   //получаем номер при нажатии
        if stillTyping {
            if (displayResultLabel.text?.characters.count)! < 20 {          //выводим на экран меньше 20 цифр
                displayResultLabel.text = displayResultLabel.text! + number //дописываем поочередно цифры в label
            }
        } else {
            displayResultLabel.text = number
            stillTyping = true
        }
    }
    
    @IBAction func twoOperandsSignPressed(_ sender: UIButton) {          //действия + - * /
        operationSign = sender.currentTitle!                             //присваиваем нажатие кнопки действий
        firstOperand = currentInput                                      //присваиваем первому оператору введеное число
        stillTyping = false
        dotIsPlaced = false                                              // у числа точка еще не стоит
    }
    
    func operateWithTwoOperands(operation: (Double, Double) -> Double) { //функция действия над операторми
        currentInput = operation(firstOperand, secondOperand)
        stillTyping = false
    }
    
    @IBAction func equalitySignPressed(_ sender: UIButton) {             // знак "="
        if stillTyping {
            secondOperand = currentInput
        }
        
        dotIsPlaced = false
        
        switch operationSign {
        case "+":
            operateWithTwoOperands{$0 + $1}
        case "-":
            operateWithTwoOperands{$0 - $1}
        case "×":
            operateWithTwoOperands{$0 * $1}
        case "÷":
            operateWithTwoOperands{$0 / $1}
        default:
            break
        }
    }
    
    @IBAction func clearButtonPressed(_ sender: UIButton) {             // кнопка удаления "C"
        firstOperand = 0
        secondOperand = 0
        currentInput = 0
        displayResultLabel.text = "0"
        stillTyping = false
        dotIsPlaced = false
        operationSign = ""
    }
    
    @IBAction func plusMinusButtonPressed(_ sender: UIButton) {         // кнопка "+/-"
        currentInput = -currentInput
    }
    
    
    @IBAction func persentageButtonPressed(_ sender: UIButton) {        // кнопка процентов
        if firstOperand == 0 {
            currentInput = currentInput / 100
        } else {
            secondOperand = firstOperand * currentInput / 100
        }
        stillTyping = false
    }
    
    
    @IBAction func squareRootButtonPressed(_ sender: UIButton) {        // кнопка квадратного кореня
        currentInput = sqrt(currentInput)
    }
    
    
    @IBAction func dotButtonPressed(_ sender: UIButton) {               // точка
        if stillTyping && !dotIsPlaced {                                // если мы пишем и точки еще нет
            displayResultLabel.text = displayResultLabel.text! + "."    // дописываем точку
            dotIsPlaced = true                                          // говорим что ставим точку
        } else if !stillTyping && !dotIsPlaced {                        // если не пишем и точка не стоит
            displayResultLabel.text = "0."
        }
    }
}

